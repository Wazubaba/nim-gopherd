import nake
import strformat
import os

const
  name = "gopherd"
  src = "src/main.nim"
  rebuild_on = @[src, "src/io.nim"]
  cache = ".nim"
  includes:seq[string] = @["submodules/nim-daemon"]
  version = "1.0.0"

proc solve_includes(): string =
  for i in includes:
    result.add(fmt"--path:{i}")

task "build", "Build the test program":
  if name.needsRefresh(rebuild_on):
    direShell(nimExe, "c", fmt"--out:{name}", fmt"--nimcache:{cache}",
               solve_includes(), src)

task "debug-build", "Build the program with debug mode enabled":
  if name.needsRefresh(rebuild_on):
    direShell(nimExe, "c", fmt"--out:{name}", fmt"--nimcache:{cache}",
               solve_includes(), "--define:test", src)

task "release", "Build the program in release mode":
    direShell(nimExe, "c", fmt"--out:{name}", fmt"--nimcache:{cache}",
               solve_includes(), "-d:release", src)

task "force-rebuild", "Force rebuild of the program":
  direShell(nimExe, "c", fmt"--out:{name}", fmt"--nimcache:{cache}",
            solve_includes(), src)

task "force-debug-rebuild", "Force rebuild of the program":
  direShell(nimExe, "c", fmt"--out:{name}", fmt"--nimcache:{cache}",
             solve_includes(), "--define:test", src)

task "clean", "Clean all generated build-files":
  if "nimcache".dirExists():
    removeDir("nimcache")
  
  if "gopherd".fileExists():
    removeFile("gopherd")

task "dist-clean", "Clean everything. Yes":
  runTask("clean")
  if ".nim".dirExists():
    removeDir(".nim")

  if "release".dirExists():
    removeDir("release")

  if "dist".dirExists():
    removeDir("dist")

  if "nakefile".fileExists():
    removeFile("nakefile")

task "package", "Build a release package":
  runTask("dist-clean")
  runTask("release")
  createDir("release")
  createDir("dist")
  createDir(joinPath("release", "bin"))
  createDir(joinPath("release", "etc"))
  createDir(joinPath("release", "doc"))
  copyFile(name, joinPath("release", "bin", name))
  copyFile("gopherdrc", joinPath("release", "etc", "gopherdrc"))
  copyFile("README.md", joinPath("release", "doc", "README.md"))
  direShell("tar", "-c", "-z", "-f", joinPath("dist", fmt"{name}-{version}.tgz"), "release")
  removeDir("release")
  echo fmt"Created distribution package dist/{name}-{version}.tgz"

# Linux-specific tasks
when defined(linux):
  task "install", "Install program for system-wide use":
    discard # TODO: Implement
