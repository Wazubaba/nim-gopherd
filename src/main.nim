# Gopherd - Simple gopher server written in nim
# Copyright © 2018 Wazubaba
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import strformat
import strutils
import os
import parseopt
import parsecfg

#import daemon

import server
import logging

const
  VERSION = "1.0.0"
  DEFAULT_CONFIG =
    if defined(test): "gopherdrc"
    elif "/etc/gopherdrc".fileExists(): "/etc/gopherdrc"
    elif "gopherdrc".fileExists(): "gopherdrc"
    else: joinPath(getConfigDir(), "gopherdrc")

var
  opts = initOptParser()
  daemonMode: bool
  config = DEFAULT_CONFIG
  root: string
  port: uint
  pidfile: string
  logfile: string
  noCreateConfig: bool

  overrideRoot = false
  overridePort = false
  overridePidfile = false
  overrideLogfile = false

when defined(linux):
  const
    SYSTEM_CONFIG_PATH = "/etc/gopherdrc"
    SYSTEM_LOG_PATH = "/var/log/gopherd.log"
    FALLBACK_LOG_PATH = "/tmp/gopherd.log"

  import posix
  proc interrupt(a: cint) {.noconv.} =
    echo "SIGINT intercepted - shutting down"
    quit(0)

  signal(SIGINT, interrupt)

when defined(windows):
  const
    SYSTEM_CONFIG_PATH = "gopherdrc"
    SYSTEM_LOG_PATH = "gopherd.log"
    FALLBACK_LOG_PATH = "gopherd.log"


if logfile == "":
  try:
    var fp = open(SYSTEM_LOG_PATH, fmWrite)
    fp.close()
    logfile = SYSTEM_LOG_PATH
  except IOError:
    logfile = FALLBACK_LOG_PATH


proc show_help() =
  echo fmt"Usage: {getAppFilename()} [OPTIONS]"
  echo "\t-h, --help         -  Show this help and exit"
  echo "\t-v, --version      -  Show version info and exit"
# echo "\t-d, --daemonize    -  Run as daemon"
  echo "\t-p, --port         -  Specify alternative port to use (Default is 70)"
  echo "\t-c, --config       -  Specify alternative config to use (Default is /etc/gopherdrc)"
  echo "\t-r, --data-root    -  Specify alternative data root to use (Default is set in config)"
  echo "\t-f, --pid-file     -  Specify alternative pidfile to use (Default is set in config)"
  echo "\t-l, --log-file     -  Specify alternative logfile to use (Default is set in config)"
  echo "\t--no-create-config -  Do not try to create an initial config"

proc show_version() =
  echo fmt"gopherd - {VERSION}"
  # TODO: Figure out how to inject a const with compilation date here


for kind, key, val in opts.getopt():
  case kind:
    of cmdLongOption, cmdShortOption:
      case key:
        of "help", "h": show_help(); quit(0)
        of "version", "v": show_version(); quit(0)
#        of "daemonize", "d": daemonMode = true
        of "port", "p": port = val.parseUint(); overridePort = true
        of "config", "c": config = val
        of "data-root", "r": root = val; overrideRoot = true
        of "pid-file", "f": pidfile = val; overridePidfile = true
        of "no-create-config": noCreateConfig = true
        else:
          discard
    else:
      discard

# Load configuration data, but only apply what is not already set
if config == "":
  let USER_CONFIG_PATH = joinPath(getConfigDir(), "gopherdrc")

  config = 
    if SYSTEM_CONFIG_PATH.fileExists: SYSTEM_CONFIG_PATH
    elif "gopherdrc".fileExists(): "gopherdrc"
    else: USER_CONFIG_PATH


  if config == USER_CONFIG_PATH and not USER_CONFIG_PATH.fileExists and not noCreateConfig:
    log(fmt"Generating default configuration data at '{USER_CONFIG_PATH}'", logfile, daemonMode)
    var cfg = newConfig()
    cfg.setSectionKey("", "port", "70")
    if not overridePort: port = 70

    cfg.setSectionKey("", "dataroot", "/var/gopher/")
    if not overrideRoot: root = "/var/gopher"
    
    cfg.setSectionKey("", "pidfile", "/tmp/gopherd.pid")
    if not overridePidfile: pidfile = "/tmp/gopherd.pid"

    cfg.setSectionKey("", "logfile", "/var/log/gopherd.log")
    if not overrideLogfile: logfile = "/var/log/gopherd.log"

    cfg.writeConfig(USER_CONFIG_PATH)

else:
  try:
    var cfg = loadConfig(config)
    

    if not overridePort: port = cfg.getSectionValue("", "port").parseUint()
    if not overrideRoot: root = cfg.getSectionValue("", "dataroot")
    if not overridePidfile: pidfile = cfg.getSectionValue("", "pidfile")
    if not overrideLogfile: logfile = cfg.getSectionValue("", "logfile")
  
  except IOError:
    error(fmt"Failed to open '{config}' for reading.", true, 1)

  except KeyError:
    error(fmt"Failed to parse value: {getCurrentExceptionMsg()}", true, 2)

#if daemonMode:
#  discard daemonize(pidfile)

log(fmt"Using: {config}", logfile, daemonMode)
log(fmt"Using pidfile: {pidfile}", logfile, daemonMode)
log(fmt"Started gopherd. Listening on {port}, Serving {root}:", logfile, daemonMode)

server.start(root, port, daemonMode, logfile)

# Also don't forget to investigate the xmpp protocol \o

#[
  Future goals:
    * Investigate whether we can implement some sort of private/public key
      auth system, where a server can be queried for a public key, which it
      must send, that can be compared against a stored key or a key provided
      within the link to it from another server (server A knows B's pub key,
      sends it to the client along with the URL, this is up to the client to
      ignore if it already has the key.) It is important for
      reverse-compatibility that this be entirely optional, and ignored if the
      client is not extended - perhaps in the list function simply have an
      optional /r/f/n to send the list with secure info? IDK yet...
]#


