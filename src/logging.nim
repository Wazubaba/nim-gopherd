# Gopherd - Simple gopher server written in nim
# Copyright © 2018 Wazubaba
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import strformat

proc error*(message: string, doQuit = false, code = 0) =
  try:
    stderr.writeline(message)
    if doQuit:
      quit(code)
  except IOError:
    # Okay. Something is seriously fucked... let's just kill the server because
    # it is clear something has gone terribly, terribly wrong...
    quit(3)

proc log*(message: string, logfile: string, silent: bool) =
  if not silent:
    echo message

  var fp: File
  if not fp.open(logfile, fmAppend):
    error(fmt"Failed to write log message to {logfile}: {message}")
  else:
    fp.writeLine(message)
