# Gopherd - Simple gopher server written in nim
# Copyright © 2018 Wazubaba
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncnet
import asyncdispatch
import strformat
import strutils
import os

import logging

type
  Connection = tuple[address: string, client: AsyncSocket]

var
  clients {.threadvar.}: seq[AsyncSocket]


proc handle(connection: Connection, dataroot: string, isDaemon: bool, logfile: string) {.async.} =
  var line = await connection.client.recvLine()
  if line == "\r\n": line = ""

  log(fmt"Connection from {connection.address}.", logfile, isDaemon)
  log(fmt"Got line: '{line}'", logfile, isDaemon)
  log(fmt"Request from {connection.address}", logfile, isDaemon)

  var
    path: string
    data: string

  try:
    path = joinPath(dataroot, line)
  except IndexError:
    # Fallback to the assumption this was a blank line
    path = dataroot

  if path.fileExists():
    data = path.open().readAll()
  elif joinPath(path, "gophermap").fileExists():
    data = joinPath(path, "gophermap").open().readAll()
  else:
    data = fmt"ERROR: Resource unavailable: {line}"

  # Honestly, I've been testing with lynx and whether sending the
  # final '.' after binary data is bad or not and it frankly does
  # not seem to matter to it at all...
  let isBinary =
    if data[0..3] == "ELF": true # We know it is a linux binary
    elif "\0" in data: true
    elif "\f" in data: true
    else: false

  await connection.client.send(data)
  if not isBinary:
    await connection.client.send(".\r\n")

  connection.client.close()


proc serve(dataroot: string, port: uint, isDaemon: bool, logfile: string) {.async.} =
  clients = @[]
  
  var server = newAsyncSocket()
  server.setSockOpt(OptReuseAddr, true)
  server.bindAddr(Port(port))
  server.listen()

  while true:
    let connection = await server.acceptAddr()
    clients.add(connection.client)

    asyncCheck(handle(connection, dataroot, isDaemon, logfile))


proc start*(dataroot: string, port: uint, isDaemon: bool, logfile: string) =
  asyncCheck(serve(dataroot, port, isDaemon, logfile))
  runForever()
