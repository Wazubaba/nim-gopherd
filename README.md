# G O P H E R D
A simple, light-weight web server using the [gopher protocol][1],
written in [Nim][2].

Currently, gopherd is a mono-threaded asynchronous server, but I'd
like to try to give it a user-adjustable number of threads for
handling requests as well in the future.

[1]:https://en.wikipedia.org/wiki/Gopher_%28protocol%29
[2]:https://nim-lang.org/

## Features
* Cross-platform (hopefully. Currently we lack full osx support because idk wtf the paths for that are).
* Multiple ways to configure it.
* Generally simple and hopefully easy to expand/modify in the future.


## Configuration
On first run, a new default configuration will be generated for you
in your user-configuration directory.
*	Linux: `$XDG_CONFIG/gopherdrc` aka `~/.config/gopherdrc`.
* Windows: `%APPDATA%\gopherdrc`

You can then choose to copy this to your system-wide configuration
directory.
* Linux: `/etc/gopherdrc`
* Windows: idk wtf so same folder as where the executable is.

You can also have the configuration in your current directory when
you start the server, or specify what configuration file to use via
command-line arguments.

Everything that can be defined in the config can also be overridden
by command-line arguments.

## Usage
Gopherd will look in the directory you are serving for a gophermap
file. This is a plain-text file in gophermap format(see example data)
that maps the paths a user can follow.

Other than the root directory, Gopherd will test to see if the
requested target is a file, and send that, otherwise it will assume
that the target is a directory and try to send the gophermap file
in that directory. Failing those it will send an error stating that
the resource is unavailable.

Gopherd atm has a rather simple logging system. I intend to improve
this later on, amongst other things.
